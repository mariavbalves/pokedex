import logo from './logo.svg';
import './App.css';
import {useState} from "react";
import Axios from "axios";
import {BrowserRouter, Route, Switch,Link} from 'react-router-dom';
import MainPage from './mainPage';
import React from 'react'; 
import PokemonPage from './PokemonPage';

export default class App extends React.Component {
    render() {
        return <div>
          <BrowserRouter>
            <Switch>
             <Route exact path="/">
              <MainPage/>
             </Route>  
             <Route path="/mainPage">
               <MainPage/>
             </Route>
             <Route path="/PokemonPage">
               <PokemonPage/>
             </Route>
            </Switch>
        </BrowserRouter>
        </div>;
    }
}