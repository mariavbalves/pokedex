import "./App.css";
import { useState } from "react";
import Axios from "axios";


import {BrowserRouter, Link, Route, Switch} from 'react-router-dom';

import 'bootstrap/dist/css/bootstrap.min.css';
import {
    Card,
    Col,
    Container,
    Image,
    Row
} from "react-bootstrap";
import React from 'react';

export default class PokemonPage extends React.Component {
    render() { 
        return  <div>
            <h4>Hello, User!</h4>

                     <Link to='./mainPage'>
                <div><Image
                    src="https://icons-for-free.com/iconfiles/png/512/home+house+icon-1320087051499461146.png"
                    width={"60px"} height={"60px"} roundedCircle/>main page</div>
        </Link>
            
            
             <Card className="text-center PokemonPagebody">

                <Card.Header>
                </Card.Header>

                <Card.Body className="PokemonPagebody">

                    <Card.Text>
                        <Container >
                            <h1><Image
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Pok%C3%A9_Ball_icon.svg/2052px-Pok%C3%A9_Ball_icon.svg.png"
                    width={"60px"} height={"60px"} roundedCircle/>The Pokemon FAN PAGE <Image
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Pok%C3%A9_Ball_icon.svg/2052px-Pok%C3%A9_Ball_icon.svg.png"
                    width={"60px"} height={"60px"} roundedCircle/></h1>

                            <p>This page contains information about Pokemon and see funfacts about it. </p>


                        </Container>
                        <br/>
                        <br/>
                        <Container>
                        
                            <Container  style={{width: '50rem'}} className="justify-content-lg-center ">
                                <Row>

                                    <Col xs={12} md={12}>
                                        <Image
                                            src="https://switch-brasil.com/wp-content/uploads/2020/05/F5E4C338-6452-4831-9886-595055D3942F-750x430.png"
                                            width={"800px"} height={"400px"} rounded/>
                                        <h2>The Origin</h2>
                                        <p>
                                        The franchise began as Pocket Monsters: Red and Green (later 
                                        released outside of Japan as Pokémon Red and Blue), a pair of 
                                        video games for the original Game Boy handheld system that were
                                         developed by Game Freak and published by Nintendo in February 1996.</p>

                                        <br/>
                                        <br/>
                                    </Col>
                                    <Col xs={12} md={12}>
                                        <Image src="http://i.imgur.com/dcujjt6.jpg"
                                               width={"400px"} height={"400px"} rounded/>

                                        <h2>The Inventor</h2>

                                        <Col md={12} >
                                        <p>
                                        Pokémon was invented by a Japanese man named Satoshi Tajiri and his friend
                                         Ken Sugimori, who is an illustrator. Back in 1982 Satoshi started a gaming 
                                         magazine together with his friends called Game Freak, but after a while he 
                                         decided to start making his own video games, instead of writing about them.</p>
                                        </Col>

                                    </Col>
                                      <Col xs={12} md={12} style={{padding: '20px'}}>
                                        <Image src="https://img.redbull.com/images/c_fill,g_auto,w_2190,h_1459/q_auto,f_auto/redbullcom/2017/09/13/f5abb4bc-79a4-4d8a-9f99-d094ed7eb4ab/rhydon-in-early-concept-designs.jpg"
                                               width={"700px"} height={"400px"} rounded/>

                                        <h2>The First Pokemon ever designed</h2>

                                        <Col md={12} >
                                        
                                        <p>
                                        It may be entry #112 in the Pokédex, but according to Ken Sugimori – the primary designer for the Pokémon games
                                        – Rhydon was the first Pokémon ever created. This is also the reason why sprites of Rhydon were so widespread
                                         in the original games. In that same interview, Sugimori mentions Lapras and Clefairy as some of the other 
                                         earliest Pokémon designs to join Rhydon.</p>
                                        </Col>

                                    </Col>


                                </Row>

                            </Container>
                        </Container>

                        <br/>
                        <br/>
                        <Container>
                            <h2>Want more? </h2>
                            <p>Check later this page for more and more updates. Pika Pika!!</p>
                             <Image src="https://media4.giphy.com/media/fSvqyvXn1M3btN8sDh/giphy.gif"
                                               width={"700px"} height={"400px"} rounded/> 
                        </Container>


                    </Card.Text>

                    </Card.Body>
                    <Card.Footer className="text-muted"> 
                        
                    </Card.Footer>
                    </Card>
        </div>

                  
    }
}
 
